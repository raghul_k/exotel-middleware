const axios = require("axios");
class FreshsalesProxy {
    static createAxiosInstance(baseURL, apiKey) {
        return axios.create({
            baseURL,
            headers: {
                Authorization: `Token token=${apiKey}`,
                'Content-Type': 'application/json'
            },
        });
    }
    constructor(domain = DOMAIN, apiKey) {
        this.baseURL = `https://${domain}/api`;
        this.apiKey = apiKey;
        this.request = this.constructor.createAxiosInstance(this.baseURL, this.apiKey);
    }
    createFreshworksCrmCallLog(callLogBody) {
        return this.request.post(`/phone_calls`, callLogBody);
    }
    getFreshworksCrmContactByPhone(number) {
        return this.request.get(`/search?q=${number}&include=contact`);
    }
}
exports.FreshsalesProxy = FreshsalesProxy;