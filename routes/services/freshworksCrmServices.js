const { FreshsalesProxy } = require("../proxies/freshworksCrmProxy");


exports.createFreshworksCrmCallLog = async(freshdeskDomain, freshdeskAPIKey, callLogBody) => {
    const freshWorksCrmProxy = new FreshsalesProxy(freshdeskDomain, freshdeskAPIKey);
    try {
        const status = await freshWorksCrmProxy.createFreshworksCrmCallLog(callLogBody);
        return status.data.phone_calls;
    } catch (err) {
        console.log(err.response.data);
    }
};
exports.getFreshworksCrmContactByPhone = async(freshdeskDomain, freshdeskAPIKey, number) => {
    const freshWorksCrmProxy = new FreshsalesProxy(freshdeskDomain, freshdeskAPIKey);
    try {
        const status = await freshWorksCrmProxy.getFreshworksCrmContactByPhone(number);
        return status.data;
    } catch (err) {
        console.log(err.response.data);
    }
};