const fs = require('fs');

module.exports = {
    getPlayer: (req, res) => {
        console.log("data---->");
        const queryId = req.params.id;
        let usernameQuery = "SELECT * FROM `players` WHERE id = '" + queryId + "'";
        db.query(usernameQuery, (err, result) => {
            res.send(result);
        });
    },
    addPlayer: (req, res) => {
        let first_name = req.body.first_name;
        let last_name = req.body.last_name;
        let position = req.body.position;
        let number = req.body.number;
        let username = req.body.user_name;
        let usernameQuery = "SELECT * FROM `players` WHERE user_name = '" + username + "'";
        db.query(usernameQuery, (err, result) => {
            if (err) {
                console.log("err---->", err);
                return res.status(500).send(err);
            }
            if (result.length > 0) {
                const message = 'Username already exists';
                res.send(message);
            } else {
                let query = "INSERT INTO `players` (first_name, last_name, position, number, user_name) VALUES ('" + first_name + "', '" + last_name + "', '" + position + "', '" + number + "',  '" + username + "')";
                db.query(query, (err, result) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.send(result);
                });
            }
        });
    },
    updatePlayer: (req, res) => {
        console.log("start")
        let playerId = req.params.id;
        let first_name = req.body.first_name;
        let last_name = req.body.last_name;
        let position = req.body.position;
        let number = req.body.number;
        let query = "UPDATE `players` SET `first_name` = '" + first_name + "', `last_name` = '" + last_name + "', `position` = '" + position + "', `number` = '" + number + "' WHERE `players`.`id` = '" + playerId + "'";
        // db.query(query, (err, result) => {
        //     if (err) {
        //         return res.status(500).send(err);
        //     }
        //     res.send(result);
        // });
    },
    deletePlayer: (req, res) => {
        console.log("deleteeeeeeeeeeeee")
        let playerId = req.params.id;
        let deleteUserQuery = 'DELETE FROM players WHERE id = "' + playerId + '"';
        db.query(deleteUserQuery, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.send(result);
        });
    }
};