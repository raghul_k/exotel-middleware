const { createFreshworksCrmCallLog, getFreshworksCrmContactByPhone } = require("./services/freshworksCrmServices")
const { queryResponse } = require("./services/mySqlServices")
module.exports = {
    ExotelAndFreshworksAccountDetails: (req, res) => {
        const requestMethod = req.method;
        const { freshdeskDomain, freshdeskAPIKey, apiKey, exotelAccountSid, exotelAPIKey, exotelAPIToken, exotelAccountRegion, createdAt, modifiedAt } = req.body;
        let query = "";
        if (requestMethod == "POST") {
            query = "INSERT INTO `accounts` (freshdeskDomain, freshdeskAPIKey, apiKey, exotelAccountSid, exotelAPIKey,exotelAPIToken,exotelAccountRegion,createdAt,modifiedAt) VALUES ('" + freshdeskDomain + "', '" + freshdeskAPIKey + "', '" + apiKey + "', '" + exotelAccountSid + "',  '" + exotelAPIKey + "','" + exotelAPIToken + "','" + exotelAccountRegion + "','" + createdAt + "','" + modifiedAt + "')";
        } else if (requestMethod == "PUT") {
            query = "UPDATE `accounts` SET `freshdeskAPIKey` = '" + freshdeskAPIKey + "', `apiKey` = '" + apiKey + "', `exotelAccountSid` = '" + exotelAccountSid + "', `exotelAPIKey` = '" + exotelAPIKey + "', `exotelAPIToken` = '" + exotelAPIToken + "', `exotelAccountRegion` = '" + exotelAccountRegion + "', `createdAt` = '" + createdAt + "', `modifiedAt` = '" + modifiedAt + "' WHERE `accounts`.`freshdeskDomain` = '" + freshdeskDomain + "'";
        };
        db.query(query, (err, result) => {
            if (err) {
                res.send(err);
                return (err);
            };
            res.send(result);
            return result;
        });
    },
    exotelPopEvents: (req, res) => {
        const { ticketId, callSid, duration, accountSid, recordingUrl, callFrom, callTo, dialWhomNumber, callDirection, startTime, agentId, callState, contactName, createdBy, modifiedBy, contactId, createdAt, modifiedAt } = req.body;
        // let query = "SELECT * FROM `calls` WHERE callSid = '" + callSid + "'";
        // console.log("query------->", query);
        // db.query(query, (err, result) => {
        //     if (err) {
        //         res.send(err);
        //         return (err);
        //     };
        // if (result.length) {
        //     let queryParams = "UPDATE `calls` SET `ticketId` = '" + ticketId + "', `callState` = '" + callState + "', `callDirection` = '" + callDirection + "', `duration` = '" + duration + "', `recordingUrl` = '" + recordingUrl + "', `callFrom` = '" + callFrom + "', `callTo` = '" + callTo + "', `startTime` = '" + startTime + "', `accountSid` = '" + accountSid + "', `contactId` = '" + contactId + "', `contactName` = '" + contactName + "', `agentId` = '" + agentId + "', `dialWhomNumber` = '" + dialWhomNumber + "', `createdBy` = '" + createdBy + "'`modifiedBy` = '" + modifiedBy + "'`createdAt` = '" + createdAt + "'`modifiedAt` = '" + modifiedAt + "' WHERE `calls`.`callSid` = '" + callSid + "'";
        //     db.query(queryParams, (error, results) => {
        //         if (error) {
        //             console.log("queryParams update error------->", error);
        //             res.send(err);
        //             return (error);
        //         };
        //         console.log("queryParams update results------->", results);
        //         res.send(results);
        //         return results;
        //     });
        // } else {
        let queryParams = "INSERT INTO `calls`(`ticketId`, `callSid`, `callState`, `callDirection`, `duration`, `recordingUrl`, `callFrom`, `callTo`, `startTime`, `accountSid`, `contactId`, `contactName`, `agentId`, `dialWhomNumber`, `createdBy`, `modifiedBy`, `createdAt`, `modifiedAt`) VALUES ('" + ticketId + "','" + callSid + "','" + callState + "','" + callDirection + "','" + duration + "','" + recordingUrl + "','" + callFrom + "','" + callTo + "','" + startTime + "','" + accountSid + "','" + contactId + "','" + contactName + "','" + agentId + "','" + dialWhomNumber + "','" + createdBy + "','" + modifiedBy + "','" + createdAt + "','" + modifiedAt + "')";
        db.query(queryParams, (error, results) => {
                if (error) {
                    console.log("queryParams insert error------->", error);
                    res.send(error);
                    return (error);
                };
                console.log("queryParams insert results------->", results);
                res.send(results);
                return results;
            })
            // }
            // });
    },
    freshworksUserDetailsStore: (req, res) => {
        const { accountSid, agentId, agentName, exotelNumber, exotelVirutalnumber, isOutboundEnable, freshdeskDomain, createdAt, modifiedAt } = req.body;
        let query = "INSERT INTO `usermapping`(`accountSid`, `agentId`, `agentName`, `exotelNumber`, `exotelVirutalnumber`, `isOutboundEnable`, `freshdeskDomain`, `createdAt`,  `modifiedAt`) VALUES ('" + accountSid + "','" + agentId + "','" + agentName + "','" + exotelNumber + "','" + exotelVirutalnumber + "','" + isOutboundEnable + "','" + freshdeskDomain + "','" + createdAt + "','" + modifiedAt + "')";
        db.query(query, (err, result) => {
            if (err) {
                res.send(err);
                return (err);
            };
            res.send(result);
            return result;
        });
    },
    exotelMissedCallEvent: async(req, res) => {
        const accountSid = req.params.accountSid;
        const { direction, from, to, recording_url, call_type, call_sid, start_time, dial_call_duration, cdial_call_status, current_time } = req.body;
        const query = "SELECT * FROM `accounts` WHERE exotelAccountSid = '" + accountSid + "'";
        const result = await queryResponse(query);
        console.log("result--->", result);
        // db.query(query, async(err, result) => {
        //     if (err) {
        //         res.send(err);
        //         return (err);
        //     };
        // const { freshdeskDomain, freshdeskAPIKey } = result[0];
        // let phoneNumber = "";
        // let call_direction = "";
        // // need to check with suresh
        // if (direction == "incoming") {
        //     phoneNumber = to;
        //     call_direction = true;
        // } else {
        //     phoneNumber = from;
        //     call_direction = false;
        // };
        // res.send(phoneNumber);
        // const contactDetailByNumber = await getFreshworksCrmContactByPhone(freshdeskDomain, freshdeskAPIKey, phoneNumber);
        // console.log(contactDetailByNumber);
        // let callLogBody = "";
        // if (contactDetailByNumber.length) {
        //     console.log("contact is already there-------->");
        //     const contactId = contactDetailByNumber[0].id;
        //     callLogBody = JSON.stringify({
        //         "phone_call": {
        //             "call_direction": call_direction,
        //             "targetable_type": "contact",
        //             "targetable": {
        //                 "id": contactId
        //             },
        //             "note ": {
        //                 "description": recording_url
        //             }
        //         }
        //     });
        // } else {
        //     console.log("contact is not there---->");
        //     callLogBody = JSON.stringify({
        //         "phone_call": {
        //             "call_direction": call_direction,
        //             "targetable_type": "contact",
        //             "targetable": {
        //                 "mobile_number": phoneNumber
        //             },
        //             "note": {
        //                 "description": recording_url
        //             }
        //         }
        //     });
        // };
        // const createCallLogResponse = await createFreshworksCrmCallLog(freshdeskDomain, freshdeskAPIKey, callLogBody);
        // console.log("createCallLogResponse--->", createCallLogResponse);
        // return createCallLogResponse;

        // });
    },
    selectEvents: (req, res) => {
        // const { callSid, callFrom, callTo, dialWhomNumber, callDirection, startTime, agentId, callState } = req.body;
        // let query = "";
        // query = "INSERT INTO `calls` (callSid, callFrom, callTo, dialWhomNumber, callDirection,startTime,agentId,callState) VALUES ('" + callSid + "', '" + callFrom + "', '" + callTo + "', '" + dialWhomNumber + "',  '" + callDirection + "','" + startTime + "','" + agentId + "','" + callState + "')";
        // console.log("query------->", query);
        query = "SELECT * FROM `calls`";
        db.query(query, (err, result) => {
            if (err) {
                res.send(err);
                return (err);
            };
            res.send(result);
            return result;
        });
    },
};