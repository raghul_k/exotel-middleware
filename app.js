const express = require("express");
const bodyParser = require("body-parser");
const ngrok = require("ngrok");
const app = express();
const routing = require("./core/routes/router");
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 8080;
global.io = io;
app.set("PORT", process.env.PORT || port);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", routing);
io.on('connection', function (socket) {
    console.log('A user connected');
    socket.on('disconnect', function () {
        console.log('A user disconnected');
    });
});
server.listen(port, () => {
    console.log(`http://localhost:${port}`);
});


ngrok.connect(
    {
        proto: "http",
        addr: port,
    },
    (err, url) => {
        if (err) {
            console.error("Error while connecting Ngrok", err);
            return new Error("Ngrok Failed");
        } else {
            console.log("Tunnel Created -> ", url);
            console.log("Tunnel Inspector ->  http://127.0.0.1:4040");
        }
    }
);