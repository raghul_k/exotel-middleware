const mysql = require("mysql");
const dbSocketPath = process.env.DB_SOCKET_PATH || "/cloudsql";
const pool = mysql.createPool({
    connectionLimit: 10,
    // user: process.env.DB_USER, // e.g. 'my-db-user'
    // password: process.env.DB_PASS, // e.g. 'my-db-password'
    // database: process.env.DB_NAME, // e.g. 'my-database'
    // socketPath: `${dbSocketPath}/${process.env.CLOUD_SQL_CONNECTION_NAME}`,
    host: "freshwork.cpg73fwbrb4a.ap-south-1.rds.amazonaws.com",
    user: "freshwork",
    password: "8RAfTMpR,7",
    port: "3306",
    database: "freshwork_rds",
});
class Repository {
    async prepareQuery(query, parameters) {
        console.log("----------------------------prepareQuery called-----------------------------");
        return new Promise((resolve, reject) => {
            pool.getConnection(function (err, connection) {
                connection.query(query, parameters, (err, result) => {
                    if (err) {
                        console.log("-------------------- while prepare query errrrrrrrr------------------------------>", err);
                        reject(err);
                    };
                    connection.release();
                    resolve(result);
                });
            });
        });
    }
};
exports.Repository = Repository;