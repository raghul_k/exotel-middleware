const { FreshWorksProxy } = require("../proxies/freshworks");
const { Repository } = require("../repositories/sql");
const { SqlServices } = require("../services/sql");
const { Helper } = require("../helper/helper");
const moment = require("moment");
const DATE_FORMAT = "YYYY-MM-DD HH:mm:ss";
const modifiedDateAndTime = moment.utc().format(DATE_FORMAT);
class FreshWorksService {
    async callEvent(req, res) {
        const repository = new Repository();
        const sqlServices = new SqlServices();
        const accountSid = req.params.accountSid;
        let commonObject;
        const objectLength = Object.keys(req.query).length;
        if (objectLength) {
            commonObject = req.query;
            commonObject.CallFrom = commonObject.CallFrom.slice(commonObject.CallFrom.length - 10);
            commonObject.CallTo = commonObject.CallTo.slice(commonObject.CallTo.length - 10);
        } else {
            commonObject = req.body;
            commonObject["CallFrom"] = commonObject.From;
            commonObject["CallTo"] = commonObject.To;
            commonObject["DialCallStatus"] = commonObject.Status;
            commonObject["DialWhomNumber"] = commonObject.From;
            commonObject["DialCallDuration"] = commonObject.Legs[1].OnCallDuration;
            commonObject.CallFrom = commonObject.CallFrom.slice(commonObject.CallFrom.length - 10);
            commonObject.CallTo = commonObject.CallTo.slice(commonObject.CallTo.length - 10);
            commonObject.DialWhomNumber = commonObject.DialWhomNumber.slice(commonObject.DialWhomNumber.length - 10);
            const getUserMappingQuery = "SELECT * FROM usermapping WHERE exotelNumber LIKE ?";
            const getUserMappingQueryParams = '%' + [commonObject.CallFrom];
            const userMappingDetails = await repository.prepareQuery(getUserMappingQuery, getUserMappingQueryParams);
            console.log("Outbound call details --->>>", userMappingDetails);
            commonObject["agentId"] = userMappingDetails[0].agentId;
            await this.socketResponse(userMappingDetails, req);
        }
        const accountsQuery = "SELECT * FROM accounts WHERE exotelAccountSid = ? ";
        const accountsQueryParams = [accountSid];
        const result = await repository.prepareQuery(accountsQuery, accountsQueryParams);
        const accountResult = result[0];
        let phoneNumber = "";
        let call_direction = "";
        if (commonObject.Direction == "incoming") {
            phoneNumber = commonObject.CallFrom;
            call_direction = true;
        } else {
            phoneNumber = commonObject.CallTo;
            call_direction = false;
        };
        const callsQuery = "SELECT * FROM calls WHERE callSid = ? ";
        const callsQueryParams = [commonObject.CallSid];
        const callsResult = await repository.prepareQuery(callsQuery, callsQueryParams);
        if (accountResult.type == "freshworksCrm") {
            const freshworksObject = await this.freshworksCrmAccount(accountResult, phoneNumber, call_direction, commonObject.RecordingUrl);
            if (callsResult.length) {
                const query = "UPDATE calls SET  callDirection = ? , callFrom = ? , callTo = ? , recordingUrl = ? , startTime = ? , duration = ? , callState = ? , contactId = ?, contactName = ?, callLogId = ?,modifiedAt = ? WHERE callSid = ? ";
                const queryParams = [commonObject.Direction, commonObject.CallFrom, commonObject.CallTo, commonObject.RecordingUrl, commonObject.StartTime, commonObject.DialCallDuration, commonObject.DialCallStatus, freshworksObject.moduleId, freshworksObject.moduleName, freshworksObject.callLogId, modifiedDateAndTime, commonObject.CallSid];
                const updateCallResult = await repository.prepareQuery(query, queryParams);
                return updateCallResult;
            } else {
                const query = "INSERT INTO calls(callSid,callState,callDirection,callFrom,callTo,agentId, dialWhomNumber,accountSid,createdAt,modifiedAt,type,recordingUrl,duration,startTime,contactId,contactName,callLogId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                const queryParams = [commonObject.CallSid, commonObject.Status, commonObject.Direction, commonObject.CallFrom, commonObject.CallTo, commonObject.agentId, commonObject.DialWhomNumber, accountSid, modifiedDateAndTime, modifiedDateAndTime, accountResult.type, commonObject.RecordingUrl, commonObject.DialCallDuration, commonObject.StartTime, freshworksObject.moduleId, freshworksObject.moduleName, freshworksObject.callLogId];
                const createCallResult = await repository.prepareQuery(query, queryParams);
                return createCallResult;
            }
        } else if (accountResult.type == "freshsales") {
            const freshworksObject = await this.freshsalesAccount(accountResult, phoneNumber, call_direction, commonObject.RecordingUrl);
            if (callsResult.length) {
                const query = "UPDATE calls SET  callDirection = ? , callFrom = ? , callTo = ? , recordingUrl = ? , startTime = ? , duration = ? , callState = ? , contactId = ?, contactName = ?, callLogId = ?,modifiedAt = ? WHERE callSid = ? ";
                const queryParams = [commonObject.Direction, commonObject.CallFrom, commonObject.CallTo, commonObject.RecordingUrl, commonObject.StartTime, commonObject.DialCallDuration, commonObject.DialCallStatus, freshworksObject.moduleId, freshworksObject.moduleName, freshworksObject.callLogId, modifiedDateAndTime, commonObject.CallSid];
                const updateCallResult = await repository.prepareQuery(query, queryParams);
                return updateCallResult;
            } else {
                const query = "INSERT INTO calls(callSid, callState, callDirection, callFrom, callTo, agentId, dialWhomNumber,accountSid,createdAt,modifiedAt,type,recordingUrl,duration,startTime,contactId,contactName,callLogId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                const queryParams = [commonObject.CallSid, commonObject.Status, commonObject.Direction, commonObject.CallFrom, commonObject.CallTo, commonObject.agentId, commonObject.DialWhomNumber, accountSid, modifiedDateAndTime, modifiedDateAndTime, accountResult.type, commonObject.RecordingUrl, commonObject.DialCallDuration, commonObject.StartTime, freshworksObject.moduleId, freshworksObject.moduleName, freshworksObject.callLogId];
                const createCallResult = await repository.prepareQuery(query, queryParams);
                return createCallResult;
            }
        };
    }
    async createCallLog(req, res) {
        try {
            const { note, phone, CallSid, callDirection, name } = req.body;
            const phoneNumber = phone;
            const repository = new Repository();
            const accountSid = req.params.accountSid;
            const freshworksAccountType = req.params.freshworksAccountType;
            const accountsQuery = "SELECT * FROM `accounts` WHERE exotelAccountSid = ? AND type = ?";
            const accountsQueryParams = [accountSid, freshworksAccountType];
            const result = await repository.prepareQuery(accountsQuery, accountsQueryParams);
            const helperFile = new Helper();
            const { freshworksDomain, freshworksAPIKey } = result[0];
            const freshWorksCrmProxy = new FreshWorksProxy(freshworksDomain, freshworksAPIKey);
            if (freshworksAccountType == "freshworksCrm") {
                const contactDetails = await freshWorksCrmProxy.getFreshworksContactByPhone(phone);
                const contactId = await helperFile.getContactId(result[0], contactDetails, phoneNumber, name);
                const callLogBody = helperFile.callLogBodyCreate(callDirection, "contact", contactId, note);
                const createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
                return createCallLogResponse.data;
            } else if (freshworksAccountType == "freshsales") {
                const contactDetails = await freshWorksCrmProxy.getFreshworksContactByPhone(phone);
                const contactDetailsArray = contactDetails.data;
                let createCallLogResponse = ""
                if (contactDetailsArray.length) {
                    let contactId = contactDetailsArray[0].id;
                    if (contactDetailsArray[0].name != name) {
                        let contactBody = await helperFile.contactUpdateBody("", name);
                        await freshWorksCrmProxy.updateContact(contactId, contactBody);
                    };
                    const callLogBody = helperFile.callLogBodyCreate(callDirection, "Contact", contactId, note);
                    createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
                } else {
                    const contactBody = helperFile.contactBodyWithNameCreate(name, phone);
                    const createdContactResponse = await freshWorksCrmProxy.createFreshworksContact(contactBody);
                    let contactId = createdContactResponse.data.contact.id;
                    const callLogBody = helperFile.callLogBodyCreate(callDirection, "contact", contactId, note);
                    createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
                };
                return createCallLogResponse.data;
            };
        } catch (err) {
            return err;
        };
    }
    async freshworksCrmAccount(result, phoneNumber, call_direction, RecordingUrl) {
        const freshWorksCrmProxy = new FreshWorksProxy(result.freshworksDomain, result.freshworksAPIKey);
        const helperFile = new Helper();
        const contactDetails = await freshWorksCrmProxy.getFreshworksContactByPhone(phoneNumber);
        const contactId = await helperFile.getContactId(result, contactDetails, phoneNumber);
        const callLogBody = helperFile.callLogBodyCreate(call_direction, "contact", contactId, RecordingUrl);
        const createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
        const callLogId = createCallLogResponse.data.phone_calls[0].id;
        const contactFirstName = createCallLogResponse.data.contacts[0].first_name;
        const contactLastName = createCallLogResponse.data.contacts[0].last_name;

        const contactName = helperFile.nameNullCheck(contactFirstName, contactLastName);
        const freshworksObject = helperFile.freshworksObjectCreate(contactId, callLogId, contactName);
        return freshworksObject;
    }
    async freshsalesAccount(result, phoneNumber, call_direction, RecordingUrl) {
        const freshWorksCrmProxy = new FreshWorksProxy(result.freshworksDomain, result.freshworksAPIKey);
        const helperFile = new Helper();
        const contactDetails = await freshWorksCrmProxy.getFreshworksContactByPhone(phoneNumber);
        const contactId = await helperFile.getContactId(result, contactDetails, phoneNumber);
        if (contactId == "no contacts") {
            const leadDetails = await freshWorksCrmProxy.getFreshworksLeadByPhone(phoneNumber);
            const leadId = await helperFile.getLeadId(result, leadDetails, phoneNumber);
            const callLogBody = helperFile.callLogBodyCreate(call_direction, "lead", leadId, RecordingUrl);
            const createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
            const callLogId = createCallLogResponse.data.phone_calls[0].id;
            console.log("createCallLogResponse.data.leads[0]---------->", createCallLogResponse.data.leads[0]);
            const firstName = createCallLogResponse.data.leads[0].first_name;
            const lastName = createCallLogResponse.data.leads[0].last_name;
            const fullName = helperFile.nameNullCheck(firstName, lastName);
            const freshworksObject = helperFile.freshworksObjectCreate(leadId, callLogId, fullName);
            return freshworksObject;
        } else {
            const callLogBody = helperFile.callLogBodyCreate(call_direction, "contact", contactId, RecordingUrl);
            const createCallLogResponse = await freshWorksCrmProxy.createFreshworksCallLog(callLogBody);
            const callLogId = createCallLogResponse.data.phone_calls[0].id;
            const firstName = createCallLogResponse.data.contacts[0].first_name;
            const lastName = createCallLogResponse.data.contacts[0].last_name;
            const fullName = helperFile.nameNullCheck(firstName, lastName);
            const freshworksObject = helperFile.freshworksObjectCreate(contactId, callLogId, fullName);
            return freshworksObject;
        }
    }
    async socketResponse(userMappingDetails, req) {
        if (userMappingDetails.length) {
            const { agentId } = userMappingDetails[0];
            let callobject = {
                "agentId": agentId,
                "callInfo": req.query
            };
            return await io.sockets.emit('outbound_call', callobject);
        } else {
            let callobject = {
                "agentId": "",
                "callInfo": req.query
            };
            return await io.sockets.emit('outbound_call', callobject);
        }
    }
};
exports.FreshWorksService = FreshWorksService;