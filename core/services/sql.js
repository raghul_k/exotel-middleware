const { Repository } = require("../repositories/sql");
const { FreshWorksProxy } = require("../proxies/freshworks");
const { Helper } = require("../helper/helper");
const moment = require("moment");
const DATE_FORMAT = "YYYY-MM-DD HH:mm:ss";
const modifiedDateAndTime = moment.utc().format(DATE_FORMAT);
class SqlServices {
    async accountsCredentials(req, res) {
        const repository = new Repository();
        const { freshworksDomain, freshworksAPIKey, exotelAccountSid, exotelAPIKey, exotelAPIToken, exotelAccountRegion, type } = req.body;
        const accountsQuery = "SELECT * FROM `accounts` WHERE freshworksDomain = ?";
        const accountsQueryParam = [freshworksDomain];
        const accountDetailsCheck = await repository.prepareQuery(accountsQuery, accountsQueryParam);
        console.log("----------------------accountDetailsCheck-------------------------", accountDetailsCheck);
        if (!accountDetailsCheck.length) {
            const query = "INSERT INTO accounts (freshworksDomain, freshworksAPIKey,  exotelAccountSid, exotelAPIKey,exotelAPIToken,exotelAccountRegion,createdAt,modifiedAt,type) VALUES (?,?,?,?,?,?,?,?,?)";
            const queryParams = [freshworksDomain, freshworksAPIKey, exotelAccountSid, exotelAPIKey, exotelAPIToken, exotelAccountRegion, modifiedDateAndTime, modifiedDateAndTime, type];
            const accountDetailsStore = await repository.prepareQuery(query, queryParams);
            return accountDetailsStore;
        } else {
            const query = "UPDATE accounts SET  freshworksAPIKey = ? , exotelAccountSid = ? , exotelAPIKey = ? , exotelAPIToken = ? , exotelAccountRegion = ?  , modifiedAt = ?,type = ? WHERE freshworksDomain = ? ";
            const queryParams = [freshworksAPIKey, exotelAccountSid, exotelAPIKey, exotelAPIToken, exotelAccountRegion, modifiedDateAndTime, type, freshworksDomain];
            const accountDetailsStore = await repository.prepareQuery(query, queryParams);
            return accountDetailsStore;
        };

    }
    async popEvents(req, res) {
        const repository = new Repository();
        let { CallSid, CallFrom, CallTo, DialWhomNumber, Direction, AgentEmail, Status } = req.query;
        CallFrom = CallFrom.slice(CallFrom.length - 10);
        CallTo = CallTo.slice(CallTo.length - 10);
        DialWhomNumber = DialWhomNumber.slice(DialWhomNumber.length - 10);
        const { accountSid } = req.params;
        const getAccountsQuery = "SELECT * FROM accounts WHERE exotelAccountSid = ?";
        const getAccountQueryParams = [accountSid];
        const accountDetails = await repository.prepareQuery(getAccountsQuery, getAccountQueryParams);
        const { freshworksDomain, freshworksAPIKey, type } = accountDetails[0];
        const freshsalesProxy = new FreshWorksProxy(freshworksDomain, freshworksAPIKey);
        const getUserMappingQuery = "SELECT * FROM usermapping WHERE exotelNumber LIKE ?";
        const getUserMappingQueryParams = '%' + [DialWhomNumber];
        const userMappingDetails = await repository.prepareQuery(getUserMappingQuery, getUserMappingQueryParams);
        await this.socketResponse(userMappingDetails, req);
        const agentEmailDetail = await freshsalesProxy.getFreshworksUsersByEmail(AgentEmail);
        let agentId = "";
        if (agentEmailDetail.data.length) {
            agentId = agentEmailDetail.data[0].id;
        };
        const getCallsQuery = "SELECT * FROM `calls` WHERE callSid = ?";
        const getCallsQueryParams = [CallSid];
        const callDetailsCheck = await repository.prepareQuery(getCallsQuery, getCallsQueryParams);
        if (!callDetailsCheck.length) {
            const query = "INSERT INTO calls( callSid, callState, callDirection, callFrom, callTo, agentId, dialWhomNumber,accountSid,createdAt,modifiedAt,type) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            const queryParams = [CallSid, Status, Direction, CallFrom, CallTo, agentId, DialWhomNumber, accountSid, modifiedDateAndTime, modifiedDateAndTime, type];
            const popEventsStore = await repository.prepareQuery(query, queryParams);
            return popEventsStore;
        } else {
            console.log("callDetailsCheck.length ----->>>", "else Update");
            const query = "UPDATE calls SET  callSid = ? , callState = ? , callDirection = ? , callFrom = ? , callTo = ? , agentId = ? , dialWhomNumber = ? , accountSid = ?, modifiedAt = ? WHERE callSid = ? ";
            const queryParams = [CallSid, Status, Direction, CallFrom, CallTo, agentId, DialWhomNumber, accountSid, modifiedDateAndTime, CallSid];
            const popEventsStore = await repository.prepareQuery(query, queryParams);
            return popEventsStore;
        };

    }
    async allCallsEvents(req, res) {
        try {
            const repository = new Repository();
            const accountSid = req.params.accountSid;
            const agentId = req.params.agentId;
            const userMappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND agentId = ?"
            const userMappingQueryParams = [accountSid, agentId];
            const usermappingResponse = await repository.prepareQuery(userMappingQuery, userMappingQueryParams);
            if (!usermappingResponse.length) {
                return usermappingResponse;
            } else {
                console.log(usermappingResponse[0]);
                const phonenumTen = usermappingResponse[0].exotelNumber;
                const phoneval = phonenumTen.slice(phonenumTen.length - 10);
                const phoneNumber = '%' + phoneval;
                const callsQuery = "SELECT * FROM calls WHERE accountSid = ? AND dialWhomNumber LIKE ?";
                const callsQueryParams = [accountSid, phoneNumber];
                const callsQueryResponse = await repository.prepareQuery(callsQuery, callsQueryParams);
                return callsQueryResponse;
            }
        }
        catch (error) {
            return { error: "No call Records" }
        }
    }
    async userMapping(req, res) {
        const repository = new Repository();
        const helper = new Helper();
        const accountSid = req.params.accountSid;
        const freshworksAccountType = req.params.freshworksAccountType;
        const objectLength = Object.keys(req.query).length;
        let usermappingQueryParams = "";
        let query = "";
        if (objectLength) {
            if (req.query.page != undefined) {
                const page = req.query.page;
                const paginationQueryParam = helper.paginationQueryParams(page);
                const usermappingPaginationQueryParams = [accountSid, freshworksAccountType, paginationQueryParam.limit, paginationQueryParam.offset];
                const usermappingPaginationQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ? LIMIT  ? OFFSET  ?";
                let userMappingPaginationResult = await repository.prepareQuery(usermappingPaginationQuery, usermappingPaginationQueryParams);
                return userMappingPaginationResult;
            } else if (req.query.agentId != undefined) {
                let queryParams = [accountSid, freshworksAccountType, req.query.agentId];
                let query = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ? AND agentId = ?";
                let userMappingResult = await repository.prepareQuery(query, queryParams);
                return userMappingResult;
            }
        } else {
            usermappingQueryParams = [accountSid, freshworksAccountType];
            query = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ?";
        }
        const moduleArray = [accountSid, freshworksAccountType];
        let mappedUsersResponse = await repository.prepareQuery(query, usermappingQueryParams);
        if (!mappedUsersResponse.length) {
            const accountsQuery = "SELECT * FROM accounts WHERE exotelAccountSid = ? AND type = ?";
            const getAccountsFromTable = await repository.prepareQuery(accountsQuery, moduleArray);
            const { freshworksDomain, freshworksAPIKey, type } = getAccountsFromTable[0];
            const freshsalesProxy = new FreshWorksProxy(freshworksDomain, freshworksAPIKey);
            const freshworksUsers = await freshsalesProxy.getFreshworksUsers();
            const freshworksUserArray = freshworksUsers.data.users;
            for (let i = 0; i < freshworksUserArray.length; i++) {
                let { id, display_name, mobile_number } = freshworksUserArray[i];
                if (mobile_number != null) {
                    mobile_number = mobile_number.slice(mobile_number.length - 10);
                };
                const userMappingQuery = 'INSERT INTO usermapping(accountSid, agentId, agentName , freshworksDomain , type,exotelNumber, createdAt,modifiedAt,isOutboundEnable) VALUES (?,?,?,?,?,?,?,?,?)';
                const userMappingArray = [accountSid, id, display_name, freshworksDomain, type, mobile_number, modifiedDateAndTime, modifiedDateAndTime, 1];
                await repository.prepareQuery(userMappingQuery, userMappingArray);
            };
        };
        mappedUsersResponse = await repository.prepareQuery(query, usermappingQueryParams);
        return mappedUsersResponse;
    }
    async userMappingBySync(req, res) {

        const repository = new Repository();
        const accountSid = req.params.accountSid;
        const freshworksAccountType = req.params.freshworksAccountType;
        const moduleArray = [accountSid, freshworksAccountType];
        const accountsQuery = "SELECT * FROM accounts WHERE exotelAccountSid = ? AND type = ?";
        const getAccountsFromTable = await repository.prepareQuery(accountsQuery, moduleArray);
        const { freshworksDomain, freshworksAPIKey, type } = getAccountsFromTable[0];
        const freshsalesProxy = new FreshWorksProxy(freshworksDomain, freshworksAPIKey);
        const freshworksUsers = await freshsalesProxy.getFreshworksUsers();
        const freshworksUserArray = freshworksUsers.data.users;
        for (let i = 0; i < freshworksUserArray.length; i++) {
            const { id, display_name } = freshworksUserArray[i];
            const getUsermappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ?AND agentId = ?";
            const getUserMappingQueryParams = [accountSid, freshworksAccountType, id];
            const getUsermappingArrayResponse = await repository.prepareQuery(getUsermappingQuery, getUserMappingQueryParams,);
            if (!getUsermappingArrayResponse.length) {
                const userMappingQuery = 'INSERT INTO usermapping(accountSid, agentId, agentName,type,freshworksDomain, createdAt,modifiedAt) VALUES (?,?,?,?,?,?,?)';
                const userMappingArray = [accountSid, id, display_name, type, freshworksDomain, modifiedDateAndTime, modifiedDateAndTime];
                await repository.prepareQuery(userMappingQuery, userMappingArray);
            };
        };
        const query = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ?";
        let mappedUsersResponse = await repository.prepareQuery(query, moduleArray);
        return mappedUsersResponse;
    }
    async userDetailsStore(req, res) {
        const repository = new Repository();
        const helper = new Helper();
        const requestBody = req.body;
        const requestParams = req.params;
        const { freshworksAccountType, accountSid } = requestParams;
        const allUserMappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ?";
        const allUserMappingQueryParams = [accountSid, freshworksAccountType];
        const allUserMappingResponse = await repository.prepareQuery(allUserMappingQuery, allUserMappingQueryParams);
        const bodyArray = await helper.bodyNullCheck(requestBody);
        console.log("---------------------bodyArray--------------------", bodyArray);
        console.log("------------------------TOTAL ARRAY---------------------", allUserMappingResponse);
        const updatedUserMappingArray = requestBody.filter(await helper.comparer(allUserMappingResponse));
        const userMappingUpdateResponse = [];
        for (let i = 0; i < updatedUserMappingArray.length; i++) {
            console.log("------------------------------------update called--------------------------");
            let { agentId, agentName, exotelNumber, exotelVirutalnumber, isOutboundEnable, freshworksDomain } = updatedUserMappingArray[i];
            const userMappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ?AND agentId = ?";
            const userMappingQueryParams = [accountSid, freshworksAccountType, agentId];
            const agentResponse = await repository.prepareQuery(userMappingQuery, userMappingQueryParams);
            if (agentResponse.length) {
                const query = "UPDATE usermapping SET  agentName = ? , exotelNumber = ? , exotelVirutalnumber = ? , freshworksDomain = ?  , modifiedAt = ?, isOutboundEnable = ?  WHERE agentId = ? ";
                const queryParams = [agentName, exotelNumber, exotelVirutalnumber, freshworksDomain, modifiedDateAndTime, isOutboundEnable, agentId];
                await repository.prepareQuery(query, queryParams);
                const object = {
                    "status": "success"
                };
                userMappingUpdateResponse.push(object);
            } else {
                const query = "INSERT INTO usermapping(accountSid,agentId,agentName,exotelNumber,exotelVirutalnumber,freshworksDomain, createdAt,modifiedAt,type,isOutboundEnable)VALUES (?,?,?,?,?,?,?,?,?,?)";
                const queryParams = [accountSid, agentId, agentName, exotelNumber, exotelVirutalnumber, freshworksDomain, modifiedDateAndTime, modifiedDateAndTime, freshworksAccountType, isOutboundEnable];
                await repository.prepareQuery(query, queryParams);
                const object = {
                    "status": "success"
                };
                userMappingUpdateResponse.push(object);
            };
        };
        console.log("userMappingUpdateResponse---------->", userMappingUpdateResponse);
        return userMappingUpdateResponse;
    }
    async socketResponse(userMappingDetails, req) {
        if (userMappingDetails.length) {
            const { agentId } = userMappingDetails[0];
            let callobject = {
                "agentId": agentId,
                "callInfo": req.query
            };
            return await io.sockets.emit('incoming_call', callobject);
        } else {
            let callobject = {
                "agentId": "",
                "callInfo": req.query
            };
            return await io.sockets.emit('incoming_call', callobject);
        }
    }
    async userSearch(req, res) {
        const repository = new Repository();
        const queryParams = req.query;
        const accountSid = req.params.accountSid;
        const freshworksAccountType = req.params.freshworksAccountType;
        if (queryParams.user != undefined) {
            const user = '%' + queryParams.user + '%';
            const usermappingQueryParams = [accountSid, freshworksAccountType, user];
            const userMappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ? AND agentname LIKE ?";
            let searchUsersResponse = await repository.prepareQuery(userMappingQuery, usermappingQueryParams);
            return searchUsersResponse;
        } else if (queryParams.number != undefined) {
            const phonenumTen = queryParams.number;
            const phoneval = phonenumTen.slice(phonenumTen.length - 10);
            const phoneNumber = '%' + phoneval + '%';
            const usermappingQueryParams = [accountSid, freshworksAccountType, phoneNumber];
            const userMappingQuery = "SELECT * FROM usermapping WHERE accountSid = ? AND type = ? AND exotelNumber LIKE ?";
            let searchUsersResponse = await repository.prepareQuery(userMappingQuery, usermappingQueryParams);
            return searchUsersResponse;
        };

    }
};
exports.SqlServices = SqlServices;