const axios = require("axios");
class FreshWorksProxy {
    static createAxiosInstance(baseURL, apiKey) {
        return axios.create({
            baseURL,
            headers: {
                Authorization: `Token token=${apiKey}`,
                'Content-Type': 'application/json'
            },
        });
    }
    constructor(domain, apiKey) {
        this.baseURL = `https://${domain}/api`;
        this.apiKey = apiKey;
        this.request = this.constructor.createAxiosInstance(this.baseURL, this.apiKey);
    }
    createFreshworksCallLog(callLogBody) {
        return this.request.post(`/phone_calls`, callLogBody);
    }
    getFreshworksContactByPhone(number) {
        return this.request.get(`/search?q=${number}&include=contact`);
    }
    getFreshworksUsers() {
        return this.request.get(`/selector/owners`);
    }
    getFreshworksLeadByPhone(number) {
        return this.request.get(`/search?q=${number}&include=lead`);
    }
    createFreshworksContact(contactBody) {
        return this.request.post(`/contacts`, contactBody);
    }
    createFreshworksLead(leadBody) {
        return this.request.post(`/leads`, leadBody);
    }
    getFreshworksUsersByEmail(email) {
        return this.request.get(`/search?q=${email}&include=user`);
    }
    updateContact(id, body) {
        return this.request.put(`/contacts/${id}`, body);
    }
}
exports.FreshWorksProxy = FreshWorksProxy;