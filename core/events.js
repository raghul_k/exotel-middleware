const { FreshWorksService } = require("./services/freshworks");
const { SqlServices } = require("./services/sql");
module.exports = {
    accountsCredentials: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.accountsCredentials(req, res);
        res.send(result);
        return result;
    },
    popEvents: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.popEvents(req, res);
        res.send(result);
        return result;
    },
    answeredCallEvent: async (req, res) => {
        const freshWorksCrmService = new FreshWorksService();
        const result = await freshWorksCrmService.callEvent(req, res);
        res.send(result);
        return result;
    },
    outBoundCallEvent: async (req, res) => {
        const freshWorksCrmService = new FreshWorksService();
        const result = await freshWorksCrmService.callEvent(req, res);
        res.send(result);
        return result;
    },
    missedCallEvent: async (req, res) => {
        const freshWorksCrmService = new FreshWorksService();
        const result = await freshWorksCrmService.callEvent(req, res);
        res.send(result)
        return result;
    },
    allCallsEvents: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.allCallsEvents(req, res);
        if (result.error) {
            res.send(result);
            return result;
        }
        else {
            let missedCallArray = [];
            const objectLength = Object.keys(req.query).length;
            if (objectLength) {
                result.forEach(element => {
                    if (element.callState != "completed" && element.callDirection != "outbound-api") {
                        missedCallArray.push(element);
                    };
                });
                res.send(missedCallArray);
                return missedCallArray;
            } else {
                res.send(result);
                return result;
            }
        }
    },
    userMapping: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.userMapping(req, res);
        res.send(result);
        return result;
    },
    userMappingBySync: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.userMappingBySync(req, res);
        res.send(result);
        return result;
    },
    userDetailsStore: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.userDetailsStore(req, res);
        res.send(result);
        return result;
    },
    createCallLog: async (req, res) => {
        const freshWorksCrmService = new FreshWorksService();
        const result = await freshWorksCrmService.createCallLog(req, res);
        res.send(result);
        return result;
    },
    userMappingsearch: async (req, res) => {
        const queryResponse = new SqlServices();
        const result = await queryResponse.userSearch(req, res);
        res.send(result);
        return result;
    }
};