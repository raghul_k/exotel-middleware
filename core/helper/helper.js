const { FreshWorksProxy } = require("../proxies/freshworks");
class Helper {
    contactBodyCreate(phoneNumber) {
        const contactBody = JSON.stringify({
            "last_name": phoneNumber,
            "mobile_number": phoneNumber
        });
        return contactBody;
    }
    contactBodyWithNameCreate(name, phoneNumber) {
        const contactBody = JSON.stringify({
            "last_name": name,
            "mobile_number": phoneNumber
        });
        return contactBody;
    }
    leadBodyCreate(phoneNumber) {
        const leadBody = JSON.stringify({
            "last_name": phoneNumber,
            "mobile_number": phoneNumber
        });
        return leadBody;
    }
    callLogBodyCreate(call_direction, moduleType, moduleId, recording_url) {
        const callLogBody = JSON.stringify({
            call_direction: call_direction,
            targetable_type: moduleType,
            targetable: {
                id: moduleId
            },
            note: {
                description: recording_url,
                targetable_type: moduleType,
                targetable_id: moduleId
            }
        });

        return callLogBody;
    }
    contactUpdateBody(first_name, last_name) {
        const contactBody = JSON.stringify({
            "first_name": first_name,
            "last_name": last_name
        });
        return contactBody;
    }
    nameNullCheck(firstName, lastName) {
        if (firstName == null) {
            firstName = ""
        };
        if (lastName == null) {
            lastName = ""
        };
        let fullName = firstName + " " + lastName;
        return fullName;
    }
    async getContactId(result, contactDetails, phoneNumber, name) {
        const freshWorksCrmProxy = new FreshWorksProxy(result.freshworksDomain, result.freshworksAPIKey);
        if (contactDetails.data.length) {
            const contactId = contactDetails.data[0].id;
            return contactId;
        } else {
            if (result.type == "freshsales") {
                return "no contacts";
            } else {
                let contactBody = "";
                if (name == undefined) {
                    contactBody = this.contactBodyCreate(phoneNumber);
                } else {
                    contactBody = this.contactBodyWithNameCreate(name, phoneNumber);
                }
                console.log("contactBody----------->", contactBody);
                const createdcontactResponse = await freshWorksCrmProxy.createFreshworksContact(contactBody);
                const contactId = createdcontactResponse.data.contact.id;
                return contactId;
            }
        }
    }
    async getLeadId(result, leadDetails, phoneNumber) {
        const freshWorksCrmProxy = new FreshWorksProxy(result.freshworksDomain, result.freshworksAPIKey);
        if (leadDetails.data.length) {
            const leadId = leadDetails.data[0].id;
            return leadId;
        } else {
            const leadBody = this.leadBodyCreate(phoneNumber);
            const createdLeadResponse = await freshWorksCrmProxy.createFreshworksLead(leadBody);
            const leadId = createdLeadResponse.data.lead.id;
            return leadId;
        }
    }
    freshworksObjectCreate(moduleId, callLogId, moduleName) {
        let freshworksObject = {
            "moduleId": moduleId,
            "callLogId": callLogId,
            "moduleName": moduleName
        };
        return freshworksObject;
    }
    paginationQueryParams(page) {
        let limit = 10;
        let offset = 0;
        if (page == 1) {
            limit = 10;
            offset = 0;
        } else {
            limit = (page * 10);
            offset = (page - 1) * 10;
        };
        const paginationObject = {
            "limit": limit,
            "offset": offset
        };
        return paginationObject;
    }
    async comparer(otherArray) {
        return function (current) {
            return otherArray.filter(function (other) {
                return other.accountSid == current.accountSid && other.agentId == current.agentId && other.agentName == current.agentName && other.exotelVirutalnumber == current.exotelVirutalnumber && other.exotelNumber == current.exotelNumber && other.isOutboundEnable == current.isOutboundEnable && other.freshworksDomain == current.freshworksDomain && other.type == current.type
            }).length == 0;
        }
    }
    async bodyNullCheck(array) {
        array.forEach(element =>
            Object.keys(element).forEach(e => {
                if (element[e] == "null") {
                    element[e] = null;
                };
            })
        );
        return array;
    }
}
exports.Helper = Helper;